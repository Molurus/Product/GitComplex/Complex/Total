# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

set(ThirdPartyLibraryPath ${CMAKE_CURRENT_SOURCE_DIR}/library/3rdparty)

set(REPROC++ ON CACHE BOOL "" FORCE)
add_subdirectory(${ThirdPartyLibraryPath}/reproc library/3rdparty/reproc)
